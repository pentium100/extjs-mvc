Ext.define('ExtJSMVC.controller.Chart1', {

    extend:'Ext.app.Controller',

    views:[
	'Chart1'
    ],

    init:function(app){

	this.control({
	    'chart1 > chart':{
		afterrender:function(component){
		    component.getStore().on('selectionchange', this.store_selectionchange);
		}
	    }
	});
    },

    store_selectionchange:function(sender, args){
	var selectedCategories = [];
	Ext.each(args.selected, function(item){
	    selectedCategories.push(item.get('category'));
	});

	var views = Ext.ComponentQuery.query('chart1 > chart');

	Ext.each(views, function(view){
	    var series = view.series.get(0);

	    series.highlight = true;
	    series.unHighlightItem();

	    Ext.each(series.items, function(item){
		if(Ext.Array.contains(selectedCategories, item.storeItem.get('category')) ){
		    series.highlightItem(item);
		}
	    });

	});
    }

});