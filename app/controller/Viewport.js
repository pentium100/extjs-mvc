Ext.define('ExtJSMVC.controller.Viewport', {

    extend:'Ext.app.Controller',

    init:function(){
	this.control({
	    'viewport > toolbar > #view1':{
		click:function(){
		    var w = new Ext.widget('view1');
		    w.show();
		}
	    },
	    'viewport > toolbar > #chart1':{
		click:function(){
		    var w = new Ext.widget('chart1');
		    w.show();
		}
	    }
	});
    }

});